# Mapa conceptual "Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)"


```plantuml 
@startmindmap 
*[#C1FFA0] Programando ordenadores \nen los 80 y ahora 

 *[#lightblue] Sistemas antiguos 
  *_ a nivel de
   * Desarrollador 
    * Limitaciones
     *_ mucha menor
      * Potencia
     *_ una muy marcada
      * Arquitectura 
     *_ restricciones por
      * cuellos de botella
       *_ por
        * Componentes
       *_ en
        * Herramientas 
    * Lenguaje
     *_ en gran su mayoria
      * Ensamblador
       *_ por
        * Eficiencia
         *_ menor sobrecarga en
          * Codigo util
       *_ para
        * Acceso a recursos 
    *_ mayor afinidad por
     * Ley de Moore
      *_ respecto a
       * Eficiencia
      *_ en periodos de
       * 2 años     

  
   
 *[#lightblue] Sistemas Actuales 
  *_ a nivel de
   * Desarrollador 
    * Lenguaje
     *_  gran parte es
      * Alto nivel
       *_ facilita
        * Programacion multiplataforma
    *_ gran cantidad de
     * Plataformas
      *_ compensa diferencias con
       * Recursos logicos y fisicos
        *_ busca homogenizar 
         * Arquitecturas de dispositivos
      *_ promueve uso de diferentes
       * Modelos de abstraccion 
        *_ disminuye 
         * Problemas de complejidad
        *_ puede causar 
         * Riesgos
          *_ en eficiencia por
           * Sobrecarga de llamadas
          *_ en funcionamiento
           * Desconocimiento de la arquitectura
    *_ mayor afinidad por
     * Ley de Gates
      *_ respecto a
       * Ineficiencia
      *_ en periodos de
       * 18 meses  
      *_ compensando
       * Ley de Moore
 
  @endmindmap 
  ```

# Mapa conceptual "Hª de los algoritmos y de los lenguajes de programación (2010)"

```plantuml 
@startmindmap 
*[#C1FFA0] Algoritmos y lenguajes de programación 

 *[#lightblue] Algoritmos
  *_ son
   * Secuencias de instrucciones
    * Finitas
    * Definidas
    * Ordenadas
    *_ se puede predecir
     * Comportamiento
  *_ manera de realizar
   * Procesos
    *_ resolver mecanicamente
     * Problemas matematicos 
    *_ reciben
     * Entrada
      *_ Generan
       * Salida  
    * Ejemplos
     * Operacion matematica
     * Receta de cocina  
  
  * Historia
   * Siglo XVII
    *_ nace el 
     * Calculador
   * Siglo XIX
    *_ primeros
     * Sistemas programables
   * Siglo XX
    *_ mas cercano a
     * Ordenadores actuales  
   

   * Tipos de coste
    * Razonables
     *_ tiempo de ejecucion
      * Crecimiento lento

    * Exponenciales
     *_ tiempo de ejecucion
      * Crecimiento exagerado
     *_ muy relacionado
      * Problemas NP

   
   


 *[#lightblue] Lenguajes de programacion
  *_ se pueden dividir
   * Paradigmas
    * Imperativo
     * Ejemplo
      * Fortran
       *_ primer lenguaje
        * Alto nivel
      * Lenguaje C
    * Funcional
     * Ejemplo
      * Lisp
       *_ trabaja en ideas de
        * Simplificacion
    * POO 
     *_ precursor
      * Simula
     * Java
     * C++ 
    * Logico
     * Ejemplo
      * Prolog  
   *_ actualmente se experimenta
    * Combinacion de paradigmas
     *_ para aprovechamiento
      * Cualidades y enfoques
  *_ es
   * Lenguaje formal
    *_ mediante
     * Serie de instrucciones
      *_ permite escribir
       * Conjunto de ordenes
      *_ establecer 
       * Acciones consecutivas 
      *_ implementar 
       * Algoritmos
  @endmindmap 
  ```

# Mapa conceptual "Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)"


```plantuml 
@startmindmap 
*[#C1FFA0] Lenguajes de Programación y\n Sistemas Informáticos

 *[#lightblue] Lenguajes de programacion
  *_ es el medio de
   * Comunicacion 
    *_ entre
     * Maquina
      *_ y 
       * Hombre
  * Evolucion 
   *_ cambia debido
    * Problemas de programacion
     * Grandes desarrollos
     * Demasiada complejidad
      *_ debido a 
       * Paradigmas no adecuados



  * Paradigma
   *_ es una manera de
    * Abordar un problema
     *_ para encontrar
      * Soluciones
   * Tipos
    * Estructurada
     *_ nos separa de 
      * Dependecias de hardware 
     *_ promueve pensar
      * Alto nivel 
     *_ ofrece una concepcion
      * Diseño modular
     * Ejemplos
      * Basic
      * C
      * Pascal
      * Fortran 

    * Funcional
     *_ se apoya principalmente en
      * Matematicas 
     *_ promueve la
      * Recursividad  
     * Ejemplos
      * ML
      * Haskell
      * Hope  

    * Logico
     *_ se basa en
      * Expresiones logicas 
     *_ objetivo
      * Modelizar problema  
       *_ solucionar mediante
        * Inferencia
     * Ejemplos
      * Prolog

    * POO
     *_ nueva manera de concebir
      * Programacion y la realidad
       *_ a traves de
        * Abstraccion
     *_ soluciona problemas mediante
      * Construir objetos  
       *_ e implementar 
        * Relaciones entre ellos
     * Ejemplos
      * Java
      * C++
      * Python  
     
    * Programacion basada en componentes
     *_ se relaciona con
      * POO
       *_ con el fin
        * Mayor reutilizacion
     * Componente
      *_ es 
       * Conjunto de objetos 
        *_ con mayor
         * Funcionalidad
      
   
 *[#lightblue] Tendencias Actuales 
  * Programacion Concurrente
   *_ para solucionar 
    * Problemas de concurrencia
  * Programacion Distribuida
   *_ motivado por 
    * Problemas grandes y complejos
   *_ distribuye el
    * Proceso de analisis
  * Nuevos paradigmas
   *_ a traves de
    * Aspectos
  * Agentes software
   *_ son
    * Aplicaciones informaticas
     *_ con tendencias de
      * Autonomia
   *_ cuentan con
    * Habilidad social
     *_ para 
      * Integracion 
       * Dialogo
       * Cooperacion
       * Coordinacion
       * Negociacion      

  *_ marcada tendencia a
   * Abstraccion 
    *_ buscando una forma de pensar
     * Humana
    *_ asumiendo el nivel de
     * Granularidad
  
  *_ Promoviendo
   * Ingenieria de Software
    *_ para una
     * Formalizacion
   * Retos 
    *_ grandes 
     * Volumenes de informacion
     * Sistemas complejos 
   * Nuevas tecnologias  
 
  @endmindmap 
  ```
















